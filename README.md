# Angular Ecommerce app 

Techstack used are :-
 
Python Django RESTAPI
Angular 12+
angular material and angular flexlayout
Pdf library

Objective of the app:- 
implementing a simple angular app to add a product and list all products avalaibale,
export any list of products and be able to add category of items.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

App is hosted in heroku servers.
[link to app](https://pacific-island-98494.herokuapp.com) 

